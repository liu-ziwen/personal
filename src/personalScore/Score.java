package personalScore;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Score {

	public static void main(String[] args) throws Exception {
		// 读取配置文件
		Properties prop = new Properties();
		prop.load(new FileInputStream("src/total.properties"));

		// 从文件中获取基础值
		// double basic_Score_Before = Integer.parseInt(prop.getProperty("before"));
		// double basic_Score_Base = Integer.parseInt(prop.getProperty("base"));
		// double basic_Score_Test = Integer.parseInt(prop.getProperty("test"));
		// double basic_Score_Program = Integer.parseInt(prop.getProperty("program"));
		// double basic_Score_Add = Integer.parseInt(prop.getProperty("add"));

		// 将total.properties文件中的获取的基础值存入到一个Map集合中
		Map<String, Double> bScore = new HashMap<String, Double>();
		bScore.put("b_before", Double.parseDouble(prop.getProperty("before")));
		bScore.put("b_base", Double.parseDouble(prop.getProperty("base")));
		bScore.put("b_test", Double.parseDouble(prop.getProperty("test")));
		bScore.put("b_program", Double.parseDouble(prop.getProperty("program")));
		bScore.put("b_add", Double.parseDouble(prop.getProperty("add")));

		// 读取html文件
		File small_File = new File("src/small.html");
		File all_File = new File("src/all.html");

		// 设置一个Map集合，用来存已获得的经验值
		Map<String, Double> iScore = new HashMap<String, Double>();
		iScore.put("before", .0);
		iScore.put("base", .0);
		iScore.put("test", .0);
		iScore.put("program", .0);
		iScore.put("add", .0);

		// 调用方法计算最终得分
		Interception(small_File, all_File, iScore);
		cal(iScore, bScore);

	}
/**
 * 
 * @param small_File 小班课文件
 * @param all_File 大班课文件
 * @param iScore 存已获得的经验值的Map集合
 */
	private static void Interception(File small_File, File all_File, Map<String, Double> iScore) {
		// 个人实得经验分
		// double my_Before = 0;
		// double my_Base = 0;
		// double my_Test = 0;
		// double my_Program = 0;
		// double my_Add = 0;

		try {
			org.jsoup.nodes.Document document = Jsoup.parse(small_File, "UTF-8");
			org.jsoup.nodes.Document document2 = Jsoup.parse(all_File, "UTF-8");

			// 小班课
			if (document != null) {
				// 获得含有要筛选数据的部分
				Elements es = document.getElementsByAttributeValue("class", "interaction-row");
				int temp;
				// 遍历每一部分
				for (int i = 0; i < es.size(); i++) {
					// 分别获得每一块的内容
					Element rows_ChildElement = document.select("div[class=interaction-row]").get(i);
					// //测试获取是否正确
					// System.out.println(rows_ChildElement);
					// System.out.println(rows_ChildElement.child(1).child(0).select("span").get(1).toString());

					if (rows_ChildElement.select("span").get(1).toString().contains("课堂完成")) {
						if (rows_ChildElement.toString().contains("已参与")) {
							Scanner sc = new Scanner(
									rows_ChildElement.select("span").get(9).text());
							temp = sc.nextInt();
							iScore.put("base", temp + iScore.get("base"));
						}
					} else if (rows_ChildElement.select("span").get(1).toString().contains("课堂小测")) {
						if (rows_ChildElement.toString().contains("已参与")) {
							Scanner sc = new Scanner(
									rows_ChildElement.select("span").get(9).text());
							temp = sc.nextInt();
							iScore.put("test", temp + iScore.get("test"));
						} else if (rows_ChildElement.select("span").get(1).toString().contains("编程题")) {
							if (rows_ChildElement.select("span").get(8).text().contains("已参与")) {
								Scanner sc = new Scanner(rows_ChildElement.select("span").get(9).text());
								temp = sc.nextInt();
								iScore.put("program", temp + iScore.get("program"));
							}
						} else if (rows_ChildElement.select("span").get(1).toString().contains("附加题")) {
							if (rows_ChildElement.select("span").get(8).text().contains("已参与")) {
								Scanner sc = new Scanner(rows_ChildElement.select("span").get(9).text());
								temp = sc.nextInt();
								iScore.put("add", temp + iScore.get("add"));
							}
						} else {
							if (rows_ChildElement.select("span").get(1).toString().contains("课前自测")) {
								if (rows_ChildElement.toString().contains("color:#8FC31F;")) {
									Scanner sc = new Scanner(rows_ChildElement.select("span").get(12).text());
									temp = sc.nextInt();
									iScore.put("before", temp + iScore.get("before"));
								}

						}
					}
				}
			}

			// 大班课
			if (document2 != null) {
				Elements es2 = document2.getElementsByAttributeValue("class", "interaction-row");
				int temp2;
				for (int i = 0; i < es2.size(); i++)
					if (es2.get(i).select("span").get(1).toString().contains("课前自测")) {
						if (es2.get(i).toString().contains("color:#8FC31F")) {
							Scanner sc2 = new Scanner(es2.get(i).select("span").get(12).text());
							temp2 = sc2.nextInt();
							iScore.put("before", temp2 + iScore.get("before"));
						}
					}

			}

			}} catch (

		IOException e) {
			e.printStackTrace();
		}
	}

	// 计算最终成绩
	/**
	 * 
	 * @param iScore 存已获得的经验值的Map集合
	 * @param bScore 存total.properties文件中获取的基础值的Map集合
	 */
	public static void cal(Map<String, Double> iScore, Map<String, Double> bScore) {
		double my_Final_Before = iScore.get("before") / bScore.get("b_before") * 100 * 0.25;
		double my_Final_Base = iScore.get("base") / bScore.get("b_base") * 100 * 0.3 * 0.95;
		double my_Final_Test = iScore.get("test") / bScore.get("b_test") * 100 * 0.2;
		double my_Final_Program = iScore.get("program") / bScore.get("b_program") * 100 * 0.1;
		double my_Final_Add = iScore.get("add") / bScore.get("b_add") * 100 * 0.05;
		double my_Final_Score = (my_Final_Before + my_Final_Base + my_Final_Test + my_Final_Program + my_Final_Add)
				* 0.9 + 6;
		System.out.println(String.format("%.2f", my_Final_Score));
	}
}